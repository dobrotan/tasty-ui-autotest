const {
  defineConfig
} = require('cypress');

module.exports = defineConfig({
  reporter: 'cypress-mochawesome-reporter',
  "reporterOptions": {
    "reportDir": "cypress/reports",
    "inline":true,
    "charts": true,
    "overwrite": false,
    "quiet": false,
    "html": true,
    "json": false,
    "reportFilename": "[status]_[datetime]-[name]-report",
    "timestamp": "longDate",
    "reportPageTitle": "Tasty app QA autotest report"
  },
  video: false,
  retries: {
    // Configure retry attempts for `cypress run`
    // Default is 0
    "runMode": 1,
    // Configure retry attempts for `cypress open`
    // Default is 0
    "openMode": 0
  },
  e2e: {
    baseUrl: 'https://app2.abtasty.com/',
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);
    },
    "specPattern": [
      "cypress/e2e/",
      //...
    ]
  },
  'projectId': '88855dd',
  "defaultCommandTimeout": 25000,
  "responseTimeout": 60000,
  "viewportHeight": 1024,
  "viewportWidth": 1280,
  "videoUploadOnPasses": false,
  "numTestsKeptInMemory": 10,

});
