#### 0.1.0-dev - *(06/02/23)*
- added account draft test
- speed optimizations
- integrate mochawesome reporter

#### 0.0.8-dev - *(05/02/23)*
- added analytics + individual recipient tests
- some fixes

#### 0.0.5-dev - *(03/02/23)*
- initial release
- implement login autotest
