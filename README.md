## Installing :control_knobs:

- recommended dependencies: [node.js](http://nodejs.org/)
- all commands write in terminal/console
- compatibility with OS X, Linux, or Windows.
  
 > clone this repo

```bash
git clone git@bitbucket.org:
```
  

> install node_modules

```bash
npm i or yarn
```

## Run UI tests :rocket:

> open gui cypress tool and manually run tests

```
npm run cy:open
```

> console run + reporter mode

```bash
npm run cy:run
```


### Install & write tests in Cypress 🕹️
[Follow these instructions to install and write tests in Cypress.](https://on.cypress.io/installing-cypress)
### Help + Testing ⚙️

**If you get stuck, here is more help:**

* [Gitter Chat](https://gitter.im/cypress-io/cypress)
* [Cypress Docs](https://on.cypress.io)
### Changelog 📝

- [CHANGELOG.md](CHANGELOG.md)