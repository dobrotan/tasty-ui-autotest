import loginPage from "../../pages/login"
import url from "../../pages/url"


describe('Auth with SSO email', () => {

    beforeEach('Open tasty login page', () => {
        url.visitBaseUrl();
        loginPage.waitpost();
    })

    it('Login with correct email', () => {
        loginPage.ssoLoginButton();

    })
    
    after('Back to login page', () => { 
        loginPage.returnLoginLinkSSO()
    })

})