import loginPage from "../../pages/login"
import url from "../../pages/url"
import api from "../../pages/api"


describe('Auth', () => {

    beforeEach('Open Tasty login dashboard and check requests', () => {
        url.visitBaseUrl();
        loginPage.waitpost();
        api.apiDetails();
    })

    it('Incorrect login 3 times for activate Captcha', () => {
        loginPage.validEmailLogin();
        loginPage.signInButton();
        loginPage.signInButton();
        loginPage.captchaFlow();

    })
    
    after('Logout to auth dashboard', () => {
       
    })
    })

   

