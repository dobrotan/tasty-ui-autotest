const {
    faker
} = require('@faker-js/faker');
require('cypress-xpath')

const clientname = faker.name.firstName()
const clientsurname = faker.name.lastName()
const password = faker.internet.password()

const emailRandom  = faker.internet.email(clientname, clientsurname, 'tasty.com', {
    allowSpecialCharacters: false
});

class loginPage{

    elements ={
        emailField : () => cy.get('#email'),
        emailButton : () =>  cy.xpath("//input[@placeholder='name@abtasty.com']"),
        passwordField : () => cy.get('#password'),
        loginBtn : () =>  cy.xpath("//button[@type='submit']"),
        privacyLink : () =>  cy.xpath("//a[@class='LoginPage__privacyPolicyLink___G2cbf']"),
        passwordButtonLink : () =>  cy.xpath("//a[@class='LoginForm__link___qoxwy']"),
        passwordButtonClick : () =>  cy.xpath("//button[@type='submit']"),
        successNotification : () =>  cy.xpath("//div[@class='Notification__container___QGOTp Notification__success___AuI0I']"),
        successHeader : () =>  cy.xpath("//h1[@class='FormHeader__title___QM2R3']"),
        returnLoginLink : () =>  cy.xpath("//a[@class='ReturnTo__link___OQCjR']"),
        returnLoginLinkSSO : () =>  cy.xpath("//a[@class='SSOLoginPage__back___i88JC']"),
        ssoLoginButton : () =>  cy.xpath("//button[@type='button']"),
        passwordEyeButton: () =>  cy.xpath("//label[@class='_filled_1h7ag_202 _label_1h7ag_190']//*[name()='svg']"),

        waitpost : () =>  cy.intercept({
            method: "POST",
            url: "https://ariane.abtasty.com/",
          }).as("dataGetFirst"),

          captchaClick : () =>  cy.get('iframe')
          .first()
          .its('0.contentDocument.body')
          .should('not.be.undefined')
          .and('not.be.empty')
          .then(cy.wrap)
          .find('#recaptcha-anchor')
          .should('be.visible')
          .click(),
   
        
    }

    validEmailLogin(){
        this.elements.emailField().click().type('test@ravefox.in');
        this.elements.passwordField().click().type('testtest');
        this.elements.loginBtn().click();
       
    }
    forgotPassword(){
      
        this.elements.passwordButtonLink().click();
        this.elements.emailButton().click().type('test@ravefox.in');
        this.elements.passwordButtonClick().click()
        this.elements.successNotification().should('exist');
        this.elements.successHeader().contains('Sent!');

    }
    signInButton(){
        this.elements.loginBtn().click();
    }
    waitpost(){
        this.elements.waitpost().wait('@dataGetFirst').its('response.statusCode').should('equal', 200);
    }
    captchaFlow(){
       this.elements.captchaClick()
    }
    returnLoginLink(){
        this.elements.returnLoginLink().click()
    }
    returnLoginLinkSSO(){
        this.elements.returnLoginLinkSSO().click()
    }
    ssoLoginButton(){
        this.elements.ssoLoginButton().click()
        this.elements.emailButton().click().type('test@ravefox.in');
        this.elements.loginBtn().click();

    }
    emailGenerate(){
        this.elements.emailField().click().clear().type(emailRandom)
    }
    passwordGenerate(){
        this.elements.passwordField().type(password)
    }
    privacyLinkflow(){
        this.elements.privacyLink().click()
    }
    passwordEyeFlow(){
        this.elements.passwordEyeButton().click()
    }
}
module.exports = new loginPage();


