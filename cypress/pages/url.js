class url {
    // visit baseUrl defined in cypress.config.js
    visitBaseUrl() {
        cy.visit(Cypress.config('baseUrl'));
    };
};


module.exports = new url();

