
class api {
    apiDetails() {
    cy.request({
        method: 'GET',
        url: '/',
        failOnStatusCode: true,
    }).as('request')
    //Validate status code
    cy.get('@request').its('status').should('eq', 200)
    cy.get('@request').then((response) => {
        cy.log(JSON.stringify(response.body))
    })
    };};
    
module.exports = new api();
